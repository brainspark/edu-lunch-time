<?php
define('APP_NAME', 'LunchTime');
$loader = require __DIR__.'/vendor/autoload.php';
$app = new Silex\Application();
$app['autoloader'] = $app->share(function() use ($loader){
    return $loader;
});
$app['autoloader']->add(APP_NAME, __DIR__);
require __DIR__ . '/' . APP_NAME . '/Resources/config/config.php';

$app->run();