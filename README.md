Lunch Time App:
==================================

Requirements:
----------------------------------
* Symfony components

Reports
=======
/report       => Get daily group order
/user-report  => Get users order report

Cron Job
=======
0 10 * * * /usr/bin/php /var/www/lunchtime/lunchtime.webkate.com/bin/cron.php groupOrder
0 17 * * * /usr/bin/php /var/www/lunchtime/lunchtime.webkate.com/bin/cron.php push_signUpForLunch

API
===

* GET  /api/users                => Get list of users
* GET  /api/dishes               => Get daily menu
* POST /api/orders               => Create user order (user, password, order)
* GET  /api/dateLocks            => Date Locks
* POST /api/device               => Add device for push (deviceId, os - ios, android)
* POST /api/push/{type}          => Push (type - lunch, signUpForLunch)

* GET  /api/admin/dishes                => Get all dishes list
* POST /api/admin/dishes                => Create daily menu
* POST /api/admin/orders                => Create new group order
* PUT  /api/admin/orders                => Update user order
* POST /api/admin/users                 => Create new user
* GET  /api/admin/users/{login}         => Get user profile
* POST /api/admin/users/{login}         => Update user profile
* POST /api/admin/users/{login}/delete  => Delete user profile

Note PUT method is emulated using _method=put param

API Codes Response
===
0 => 'error'
1 => 'ok'
4 => 'Користувача не знайдено!'
5 => 'Час замовлень вичерпано!'
6 => 'Меню ще не готово'
7 => 'Немає замовлень для замовлення'

JSON samples
============

Base Dishes
-----------
`{
  "dishes": [
    {
      "id": 3847,
      "name": "Отбивная",
      "description": "Куриная отбивная",
      "category": "Второе",
      "price": 5,
      "image": "/images/chicken-stake.jpg",
      "tags": ["meat", "chicken"]
    }
  ]
}`

Dish
----
`{
  "id": 3847,
  "name": "Отбивная",
  "description": "Куриная отбивная",
  "category": "Второе",
  "price": 5,
  "image": "/images/chicken-stake.jpg",
  "tags": ["meat", "chicken"]
}`

User order
----------
`{
  "user": "login",
  "order": [
    {
      "id": 3847,
      "count": 1,
    }
  ]
}`

Group order
-----------
`{
  "order": [
    {
      "id": 3847,
      "count": 10,

    },
    {
      "id": 93,
      "count": 2
    }
  ]
}`

User
----
`{
  "login": "login",
  "name": "Vasia Pupkin",
  "balance": 120
}`

Users
-----
`{
  "users": [
    {
      "login": "petia",
      "name": "Петя",
      "balance": 10
    },
    {
      "login": "svydrygailo",
      "name": "Свидригайло",
      "balance": 11
    }
  ]
}`
