<?php
namespace LunchTime\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;

class AdminController implements ControllerProviderInterface
{
    private $dish;

    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        $controllers->get('/', array($this, 'adminAction'))->bind('admin');
        $controllers->get('/create_menu', array($this, 'createMenuAction'))->bind('admin.create_menu');
        $controllers->get('/dishes_list', array($this, 'dishesListAction'))->bind('admin.dishes_list');
        $controllers->get('/users_list', array($this, 'usersListAction'))->bind('admin.users_list');
        $controllers->get('/order_list/{date}', array($this, 'ordersListAction'))->bind('admin.order_list');

        $controllers->get('/updatePrc', array($this, 'updatePrcAction'))->bind('admin.updatePrc');

        $this->dish = $app['dish'];

        return $controllers;
    }

    public function adminAction(Application $app)
    {
        return $app["twig"]->render('admin/index.html.twig', array('date' => $this->getOrderDate()));
    }

    public function createMenuAction(Application $app)
    {
        return $app["twig"]->render('admin/create_menu.html.twig', array('date' => $this->getOrderDate()));
    }

    public function dishesListAction(Application $app)
    {
        return $app["twig"]->render('admin/dishes_list.html.twig');
    }

    public function usersListAction(Application $app)
    {
        return $app["twig"]->render('admin/users_list.html.twig');
    }

    public function ordersListAction(Application $app, $date)
    {
        $date = $this->getDate($date);
        $orders = $app['dish']->generateUserReport($date);

        return $app["twig"]->render('admin/order_list.html.twig', array('orders' => $orders, 'date' => $date->format(DATE_FORMAT)));
    }

    protected function getDate($date)
    {
        return \DateTime::createFromFormat(DATE_FORMAT, $date);
    }

    public function getOrderDate()
    {
        return $this->dish->getOrderDate()->format(DATE_FORMAT);
    }

    public function updatePrcAction(Application $app)
    {
        $users = $app['user']->getAllUsers(true);

        foreach ($users as $user) {
            $user['password'] = '0000';
            $app['user']->updateUser($user);
        }

        return 1;
    }
}