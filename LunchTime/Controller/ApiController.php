<?php
namespace LunchTime\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;
use Symfony\Component\HttpFoundation\Response;

class ApiController implements ControllerProviderInterface
{
    private $apiResponse;

    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];
        $controllers->get('/users', array($this, 'getUsersAction'))->bind('api.users');
        $controllers->get('/dishes', array($this, 'getDishesAction'))->bind('api.dishes');
        $controllers->post('/orders', array($this, 'postOrdersAction'))->bind('api.orders');
        $controllers->get('/dateLocks', array($this, 'getDateLocksAction'))->bind('api.dateLocks');
        $controllers->post('/device', array($this, 'postDeviceAction'))->bind('api.device');
        $controllers->post('/push/{type}', array($this, 'getPushAction'))->bind('api.push');

        $this->apiResponse = $app['api_response'];

        return $controllers;
    }

    public function getUsersAction(Application $app)
    {
        $users = array_values($app['user']->getAllUsers());

        return $this->apiResponse->response(1, array('users' => $users));
    }

    public function getDishesAction(Application $app)
    {
        $date = $app['request']->get('date');
        if (empty($date)) {
            $date = $app['dish']->getOrderDate();
        } else {
            $date = $this->getDate($date);
        }

        try {
            $app['dish']->checkDate($date);
        } catch (\Exception $error) {
            return $this->apiResponse->response(5);
        }
        $dishes = array_values($app['dish']->getAllDishes($date));
        if (0 == count($dishes)) {
            $code = 6;
        } else {
            $code = 1;
        }

        return $this->apiResponse->response($code, array('dishes' => $dishes, 'date' => $date->format(DATE_FORMAT)));
    }

    public function postOrdersAction(Application $app)
    {
        $message = null;
        $data = $app['request']->get('data');

        $fields = array('user', 'code', 'order', 'date');
        foreach ($fields as $field) {
            if (! isset($data[$field])) {
                return $this->apiResponse->response(0, array(), 'Не валідні дані. Передайте ' .$field);
            }
        }

        if (! $app['user']->correctPassword($data['user'], $data['code'])) {
            return $this->apiResponse->response(8);
        }

        try {
            $app['dish']->createOrder($data, $this->getDate($data));
            $code = 1;
        } catch (\Exception $error) {
            $code = 0;
            $message = $error->getMessage();
        }

        return $this->apiResponse->response($code, array(), $message);
    }

    public function getDateLocksAction(Application $app)
    {
        $locks = $app['persister']->findLocks();

        return $this->apiResponse->response(1, array('locks' => $locks));
    }

    public function postDeviceAction(Application $app)
    {
        $deviceId = $app['request']->get('deviceId');
        $os = $app['request']->get('os');

        if ($deviceId && $os) {
            $devices =  $app['persister']->find('devices');
            $isFind = false;
            if ($devices) {
                foreach ($devices as $device) {
                    if ($deviceId == $device['deviceId']) {
                        $isFind = true;
                        break;
                    }
                }
            }
            if (! $isFind) {
                $devices[] = array('os' => $os, 'deviceId' => $deviceId);
                $app['persister']->save('devices', $devices);
            }
            $code = 1;
        } else {
            $code = 0;
        }

        return $this->apiResponse->response($code);
    }

    public function getPushAction(Application $app, $type)
    {
        $devices =  $app['persister']->find('devices');
        switch ($type) {
            case 'lunch': {
                $body = 'Обіди привезли. Всі на обід! Бігом !!!';
            } break;
            case 'signUpForLunch': {
                $body = 'Нагадування! Замовте обід, а то будете голодні.';
            } break;
            default: {
                return $this->apiResponse->response(0);
            } break;
        }
        $app['push_notification']->sendMessages($devices, 'Обід', $body);

        return $this->apiResponse->response(1, null, 'Push повідомлення надіслано');
    }

    protected function getDate($data)
    {
        if (is_array($data) && isset($data['date'])) {
            return \DateTime::createFromFormat(DATE_FORMAT, $data['date']);
        } else {
            if ($data) {
                return \DateTime::createFromFormat(DATE_FORMAT, $data);
            } else {
                return new \DateTime();
            }
        }
    }
}