<?php
namespace LunchTime\Controller;

use Silex\Application,
    Silex\ControllerProviderInterface;
use LunchTime\Libraries\Image;

class ApiAdminController implements ControllerProviderInterface
{
    private $pathImg;
    private $apiResponse;

    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];

        $controllers->get('/dishes', array($this, 'getDishesAction'))->bind('api.admin.get.dishes');
        $controllers->post('/dishes', array($this, 'postDishesAction'))->bind('api.admin.post.dishes');
        $controllers->post('/dishes_list', array($this, 'postDishesListAction'))->bind('api.admin.post.dishes_list');

        $controllers->post('/orders', array($this, 'postOrdersAction'))->bind('api.admin.post.orders');
        $controllers->post('/order/delete', array($this, 'deleteOrdersAction'))->bind('api.admin.delete.order');
        $controllers->post('/groupOrder', array($this, 'postGroupOrderAction'))->bind('api.admin.post.group_order');

        $controllers->get('/users', array($this, 'getUsersAction'))->bind('api.admin.get.users');
        $controllers->post('/users', array($this, 'postUsersAction'))->bind('api.admin.post.user');
        $controllers->post('/users/{login}', array($this, 'updateUsersAction'))->bind('api.admin.update.user');
        $controllers->post('/users/{login}/delete', array($this, 'deleteUserAction'))->bind('api.admin.delete.user');
        $controllers->get('/users/{login}', array($this, 'getUserAction'))->bind('api.admin.get.user');

        $controllers->get('/images', array($this, 'getImagesAllAction'))->bind('api.admin.get.images_all');
        $controllers->post('/image/upload', array($this, 'postImageUploadAction'))->bind('api.admin.post.image');
        $controllers->post('/image/delete', array($this, 'postImageDeleteAction'))->bind('api.admin.post.image_delete');

        $this->apiResponse = $app['api_response'];

        $this->pathImg = __DIR__.'/../../img/';

        return $controllers;
    }

    public function getDishesAction(Application $app)
    {
        $date = $this->getDate($app['request']->get('date'));

        $base = $app['persister']->findOne('base');
        $current = $app['dish']->getAllDishes($date);

        return $app->json(array('base' => $base, 'current' => $current));
    }

    public function postDishesAction(Application $app)
    {
        $message = null;
        $request = $app['request'];
        $date = $this->getDate($request->get('date'));

        try {
            $allDishes = $app['persister']->findOne('base');
            $app['persister']->deleteAll('dish', $date);

            foreach ($request->get('dishes') as $item) {
                if (isset($item['id'])) {
                    foreach ($allDishes['dishes'] as $dish) {
                        if ($item['id'] == $dish['id']) {
                            if (isset($item['price'])){
                                $dish['price'] = $item['price'];
                            }
                            $app['dish']->createDish($dish, $date);
                        }
                    }
                }
            }

            $code = 1;
        } catch (\Exception $error) {
            $code = 0;
            $message = $error->getMessage();
        }

        return $this->apiResponse->response($code, array(), $message);
    }

    public function postDishesListAction(Application $app)
    {
        $message = null;
        $request = $app['request'];

        try {
            $dishes = $request->get('dishes');
            foreach ($dishes as $k => $dish) {
                $dishes[$k]['tags'] = explode(',', $dish['tags']);
            }
            $dishes = array_values($dishes);
            $app['persister']->saveBaseDishes($dishes);

            $code = 1;
        } catch (\Exception $error) {
            $code = 0;
            $message = $error->getMessage();
        }

        return $this->apiResponse->response($code, array(), $message);
    }

    public function postOrdersAction(Application $app)
    {
        $method = $app['request']->get('_method');
        if ($method && strtolower($method) == 'put') {
            return $this->updateOrdersAction($app);
        } else {
            return $this->createOrderAction($app);
        }
    }

    public function deleteOrdersAction(Application $app)
    {
        $id = $app['request']->get('id');
        $date = $this->getDate($app['request']->get('date'));

        if ($id && $date) {
            if ('all' == $id) {
                $app['persister']->deleteAll('order', $date);
            } else {
                $app['persister']->delete('order', array('user' => $id), $date);
            }

            $code = 1;
        } else {
            $code = 0;
        }

        return $this->apiResponse->response($code);
    }

    public function postGroupOrderAction(Application $app)
    {
        $message = null;
        $date = $this->getDate($app['request']->get('date'));

        try {
            $report = $app['dish']->generateReport($date);
            $userReport = $app['dish']->generateUserReport($date);

            if (0 == count($report)) {
                $code = 7;
            } else {
                $app['dish']->createGroupOrder($date);
                $dateText = $date->format(DATE_FORMAT);

                $app['mail']->toMaker_groupOrder(array('report' => $report, 'date' => $dateText));
                $app['mail']->toAdmins_groupOrder(array(
                    'report' => $report,
                    'userReport' => $userReport,
                    'date' => $dateText)
                );

                $code = 1;
            }
        } catch (\Exception $error) {
            $code = 0;
            $message = $error->getMessage();
        }

        return $this->apiResponse->response($code, array(), $message);
    }

    public function createOrderAction(Application $app)
    {
        $message = null;
        $request = $app['request'];
        $date = $this->getDate($request->get('date'));

        try {
            $app['dish']->createOrder($request->get('data'), $date);
            $code = 1;
        } catch (\Exception $error) {
            $code = 0;
            $message = $error->getMessage();
        }

        return $this->apiResponse->response($code, array(), $message);
    }

    public function updateOrdersAction(Application $app)
    {
        $message = null;
        $request = $app['request'];
        $date = $this->getDate($request->get('date'));

        try {
            $app['dish']->updateOrder($request->get('data'), $date);
            $code = 1;
        } catch (\Exception $error) {
            $code = 0;
            $message = $error->getMessage();
        }

        return $this->apiResponse->response($code, array(), $message);
    }

    public function getUsersAction(Application $app)
    {
        $users = array_values($app['user']->getAllUsers(true));

        return $this->apiResponse->response(1, array('users' => $users));
    }

    public function postUsersAction(Application $app)
    {
        $request = $app['request'];
        $message = null;
        $data    = null;

        try {
            $code = 1;
            $data = $app['user']
                ->createUser($request->get('user'))
                ->toResponse();
        } catch (\Exception $error) {
            $code = 0;
            $message = $error->getMessage();
        }

        return $this->apiResponse->response($code, $data, $message);
    }

    public function updateUsersAction(Application $app, $login)
    {
        $repUser = $app['user'];
        $message = null;

        try {
            if (false == $repUser->findUser($login)) {
                $code = 4;
            } else {
                $code = 1;
                $data = $app['request']->get('user');
                $data['login'] = $login;

                $repUser->updateUser($data);
            }
        } catch (\Exception $error) {
            $code = 0;
            $message = $error->getMessage();
        }

        return $this->apiResponse->response($code, null, $message);
    }

    public function getUserAction(Application $app, $login)
    {
        $user = $app['user']->findUser($login);
        if (false == $user) {
            return $this->apiResponse->response(4);
        } else {
            return $this->apiResponse->response(1, $user->toResponse());
        }
    }

    public function deleteUserAction(Application $app, $login)
    {
        $user = $app['user']->findUser($login);
        if (false == $user) {
            return $this->apiResponse->response(4);
        } else {
            $app['user']->deleteUser($login);

            return $this->apiResponse->response(1);
        }
    }

    public function getImagesAllAction(Application $app)
    {
        $images = array();
        //$files = array_reverse(glob($this->pathImg . '*.*'));
        $files = glob($this->pathImg . '*.*');
        foreach ($files as $file) {
            $images[] = 'img/'.basename($file);
        }

        return $this->apiResponse->response(1, array('images' => $images));
    }

    public function postImageUploadAction(Application $app)
    {
        $mimeTypes = array('image/gif', 'image/jpeg','image/pjpeg','image/x-png', 'image/png');

        $file = $app['request']->files->get('photo');
        if(empty($file)) {
            $this->apiResponse->response(0);
        }

        //$filename = $file->getClientOriginalName();
        $filename = time().rand(0, 99).'.jpg';
        if (in_array($file->getMimeType(), $mimeTypes)) {
            $file->move($this->pathImg, $filename);

            $imgPath = $this->pathImg.$filename;
            $image = new Image();
            $image->load($imgPath);
            $image->resizeToHeight(233);
            $image->save($imgPath);

            $code = 1;
        } else {
            $code = 9;
        }

        return $this->apiResponse->response($code);
    }

    public function postImageDeleteAction(Application $app)
    {
        $file = __DIR__.'/../../'.$app['request']->get('file');
        if (file_exists($file)) {
            unlink($file);
            $code = 1;
        } else {
            $code = 0;
        }

        return $this->apiResponse->response($code);
    }


    protected function getDate($data)
    {
        if ($data) {
            return \DateTime::createFromFormat(DATE_FORMAT, $data);
        } else {
            return new \DateTime();
        }
    }
}