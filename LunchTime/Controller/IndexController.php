<?php
namespace LunchTime\Controller;

use Silex\Application;
use Silex\ControllerProviderInterface;

class IndexController implements ControllerProviderInterface
{
    public function connect(Application $app)
    {
        $controllers = $app['controllers_factory'];
        $controllers->get('/', array($this, 'indexAction'))->bind('index');

        $controllers->get('/user-report/{date}', array($this, 'userReportAction'))->bind('user.report');
        $controllers->get('/report/{date}', array($this, 'reportAction'))->bind('report');

        $controllers->get('/test_push', array($this, 'testPushAction'))->bind('test_push');

        return $controllers;
    }

    public function indexAction(Application $app)
    {
        $date = new \DateTime();
        if ($app['persister']->dateIsLocked($date)) {
            $date = new \DateTime('+ 1 day');
        }

        return $app["twig"]->render('index.html.twig', array(
            'users' => $app['user']->getAllUsers(),
            'date' => $date->format(DATE_FORMAT)
        ));
    }

    public function reportAction(Application $app, $date)
    {
        $report = $app['dish']->generateReport($this->getDate($date));

        return $app["twig"]->render('reports/report.html.twig', array(
            'report' => $report,
            'date' => $date
        ));
    }

    public function userReportAction(Application $app, $date)
    {
        $report = $app['dish']->generateUserReport($this->getDate($date));

        return $app["twig"]->render('reports/user_report.html.twig', array(
            'report' => $report,
            'date' => $date
        ));
    }


    public function testPushAction(Application $app)
    {
        $deviceId = $app['request']->get('deviceId');

        if (! isset($deviceId)) {
            $deviceId = '8087f007fc6d4eb700c6fdbff18fea726f7dda7ccb98509fee1d3e3aba3383a1';
        }

        $app['push_notification']->sendMessage(
            array('os' => 'ios', 'deviceId' => $deviceId),
            'test', 'test'
        );

        return 1;
    }

    protected function getDate($date)
    {
        return \DateTime::createFromFormat(DATE_FORMAT, $date);
    }
}