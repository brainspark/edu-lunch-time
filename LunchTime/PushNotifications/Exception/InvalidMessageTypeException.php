<?php

namespace LunchTime\PushNotifications\Exception;

class InvalidMessageTypeException extends \RuntimeException
{
}