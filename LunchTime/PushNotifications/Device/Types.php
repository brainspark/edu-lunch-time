<?php

namespace LunchTime\PushNotifications\Device;

class Types
{
    const OS_ANDROID_C2DM = "push_notifications.os.android.c2dm";
    const OS_ANDROID_GCM = "push_notifications.os.android.gcm";
    const OS_IOS = "push_notifications.os.ios";
    const OS_MAC = "push_notifications.os.mac";
    const OS_BLACKBERRY = "push_notifications.os.blackberry";
    const OS_WINDOWSMOBILE = "push_notifications.os.windowsmobile";
    const OS_WINDOWSPHONE = "push_notifications.os.windowsphone";
}
