<?php

namespace LunchTime\PushNotifications\Service\OS;

use LunchTime\PushNotifications\Message\MessageInterface;

interface OSNotificationServiceInterface
{
    /**
     * Send a notification message
     *
     * @param  \RMS\PushNotificationsBundle\Message\MessageInterface $message
     * @return mixed
     */
    public function send(MessageInterface $message);
}
