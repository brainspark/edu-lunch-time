<?php
namespace LunchTime\Helper;


class Mail
{
    const FROM_EMAIL = 'noreply@lunchtime.webkate.com';
    const DEFAULT_SUBJECT = 'Lunch Time';

    private $templateEngine;
    private $emails;

    public function __construct($templateEngine, $emails)
    {
        $this->templateEngine = $templateEngine;
        $this->emails = $emails;
    }

    function sendMail($from, $to, $subject, $text)
    {
        $subject = iconv('utf-8', 'windows-1251', $subject);
        $text = iconv('utf-8', 'windows-1251', $text);
        $headers = "From: \"" . $from . "\"<" . $from . ">\n";
        $headers.= "X-sender: \"" . $from . "\"<" . $from . ">\n";
        $headers.= "Content-Type: text/html; charset=windows-1251\n";
        $headers.= "MIME-Version: 1.0\r\n";
        $headers.= "Content-Transfer-Encoding: 8bit\r\n";

        if (mail($to, $subject, $text, $headers))
            return 1;
        else
            return 0;
    }

    function sendMailTpl($from, $to, $subject, $name, $params = array())
    {
        $text = $this->templateEngine->render('mail/' . $name . '.html.twig', $params);

        return $this->sendMail($from, $to, $subject, $text);
    }

    public function toMaker_groupOrder($params)
    {
        $subject = self::DEFAULT_SUBJECT . ' - Замовлення на ' . $params['date'];

        return $this->sendMailTpl(self::FROM_EMAIL, $this->emails['maker'], $subject, 'toMaker_groupOrder', $params);
    }

    public function toAdmins_groupOrder($params)
    {
        $subject = self::DEFAULT_SUBJECT . ' - Замовлення на ' . $params['date'];

        return $this->sendMailTpl(self::FROM_EMAIL, $this->emails['admins'], $subject, 'toAdmins_groupOrder', $params);
    }
} 