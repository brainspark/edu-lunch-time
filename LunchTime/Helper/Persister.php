<?php
namespace LunchTime\Helper;

class Persister
{
    protected $path;

    public function __construct()
    {
        $this->path = __DIR__ . '/../Resources/data';
        if (!is_dir($this->path. '/users')) mkdir($this->path. '/users', 0777);
        if (!is_dir($this->path. '/lock')) mkdir($this->path. '/lock', 0777);
        if (!is_dir($this->path. '/dishes')) mkdir($this->path. '/dishes', 0777);
        if (!is_dir($this->path. '/orders')) mkdir($this->path. '/orders');
        if (!is_dir($this->path. '/devices')) mkdir($this->path. '/devices');
        $this->createDirOfDate(date(DATE_FORMAT));
    }

    public function createDirOfDate($data)
    {
        if (!is_dir($this->path. '/dishes/' . $data)) mkdir($this->path. '/dishes/' . $data, 0777);
        if (!is_dir($this->path. '/orders/' . $data)) mkdir($this->path. '/orders/' . $data, 0777);
    }

    public function findOne($collection, $criteria = null, $date = null)
    {
        switch ($collection) {
            case 'user':
                $document = $this->path . "/users/{$criteria}.json";
                break;

            case 'dish':
                $date = $date ? $date->format(DATE_FORMAT) : date(DATE_FORMAT);
                $document = $this->path . "/dishes/{$date}/{$criteria}.json";
                break;

            case 'order':
                $date = $date ? $date->format(DATE_FORMAT) : date(DATE_FORMAT);
                $document = $this->path . "/orders/{$date}/{$criteria}.json";
                break;

            case 'base':
                $document = $this->path . "/dishes.json";
                break;
        }

        if (file_exists($document)) {
            return json_decode(file_get_contents($document), true);
        } else {
            return false;
        }
    }

    public function find($collection, $date = null)
    {
        $path = '';
        $date = $date ? $date->format(DATE_FORMAT) : date(DATE_FORMAT);

        switch ($collection) {
            case 'user': {
                $path = $this->path . '/users';
            } break;
            case 'dish': {
                $path = $this->path . '/dishes/' . $date;
            } break;
            case 'order': {
                $path = $this->path . '/orders/' . $date;
            } break;
            case 'devices': {
                $path = $this->path . '/devices/list.json';
                return json_decode(file_get_contents($path), true);
            } break;
        }

        $result = array();
        $documents = glob($path.'/*.json');
        if ($documents) {
            foreach ($documents as $document) {
                $data = file_get_contents($document);
                $result[] = json_decode($data, true);
            }
        }

        return $result;
    }

    public function save($collection, $document, $date = null)
    {
        $date = $date ? $date->format(DATE_FORMAT) : date(DATE_FORMAT);
        $this->createDirOfDate($date);

        switch ($collection) {
            case 'user':
                $login = $document['login'];
                file_put_contents($this->path . "/users/{$login}.json", json_encode($document));
                break;

            case 'dish':
                $id = $document['id'];
                file_put_contents($this->path . "/dishes/{$date}/{$id}.json", json_encode($document));
                break;

            case 'order':
                $login = $document['user'];
                file_put_contents($this->path . "/orders/{$date}/{$login}.json", json_encode($document));
                break;

            case 'devices':
                file_put_contents($this->path . "/devices/list.json", json_encode($document));
                break;
        }
    }

    public function saveBaseDishes($documents)
    {
        file_put_contents($this->path . "/dishes.json", json_encode(array('dishes' => $documents)));
    }

    public function delete($collection, $document, $date = null)
    {
        switch ($collection) {
            case 'user':
                $login = $document['login'];
                $path = $this->path . "/users/{$login}.json";
                break;

            case 'dish':
                $id = $document['id'];
                $date = $date->format(DATE_FORMAT);
                $path = $this->path . "/dishes/{$date}/{$id}.json";
                break;

            case 'order':
                $login = $document['user'];
                $date = $date->format(DATE_FORMAT);
                $path = $this->path . "/orders/{$date}/{$login}.json";
                break;
        }
        if (file_exists($path)) {
            unlink($path);
        }
    }

    public function deleteAll($collection, $date)
    {
        switch ($collection) {
            case 'user':
                $path = $this->path . "/users/";
                break;

            case 'dish':
                $date = $date->format(DATE_FORMAT);
                $path = $this->path . "/dishes/{$date}/";
                break;

            case 'order':
                $date = $date->format(DATE_FORMAT);
                $path = $this->path . "/orders/{$date}/";
                break;
        }
        if (file_exists($path)) {
            foreach (glob($path . '*.json') as $file) {
                unlink($file);
            }
        }
    }

    public function findLocks($limit = 10)
    {
        $items = array_reverse(glob($this->path.'/lock/*'));
        $i = 0;
        if ($items) {
            foreach ($items as $item) {
                $result[] = basename($item) . ' '. file_get_contents($item);
                $i++;
                if ($i > ($limit - 1)) {
                    break;
                }
            }
        }

        return $result;
    }

    public function lockDate($date = null)
    {
        $date = $date ? $date->format(DATE_FORMAT) : date(DATE_FORMAT);
        $time = new \DateTime();

        file_put_contents($this->path . '/lock/' . $date, $time->format('H:i:s'));
    }

    public function dateIsLocked($date = null)
    {
        $date = $date ? $date->format(DATE_FORMAT) : date(DATE_FORMAT);

        return file_exists($this->path . '/lock/' . $date) ? true : false;
    }
} 