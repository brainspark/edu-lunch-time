<?php
namespace LunchTime\Helper;

class Dish
{
    protected $persist;

    public function __construct($persist = null)
    {
        $this->persist = $persist;
    }

    public function getAllDishes($date = null)
    {
        $result = array();
        $dishes = $this->persist->find('dish', $date);
        foreach ($dishes as $dish) {
            $img = $dish['image'];
            if (empty($img)) {
                $img = 'public/img/noimg2.jpg';
            }
            $dish['image'] = BASE_URL.'/'.$img;
            $result[$dish['id']] = $dish;
        }

        return $result;
    }

    public function createDish($data, $date)
    {
        $this->persist->save('dish', $data, $date);
    }

    public function createOrder($data, $date)
    {
        $this->checkDate($date);
        foreach ($data['order'] as $k => $order) {
            $data['order'][$k]['data'] =  $this->persist->findOne('dish', $order['id']);
        }

        $this->persist->save('order', $data, $date);
    }

    public function updateOrder($data, $date)
    {
        $this->persist->save('order', $data, $date);
    }

    public function getAllOrders($date = null)
    {
        return $this->persist->find('order', $date);
    }

    public function createGroupOrder($date = null)
    {
        if ($this->persist->dateIsLocked($date)) {
            throw new \Exception('Замовлення вже зроблено!');
        }

        $orders = $dishes =  array();
        foreach ($this->getAllOrders($date) as $item) {
            $orders[$item['user']] = $item;
        }
        foreach ($this->getAllDishes($date) as $item) {
            $dishes[$item['id']] = $item;
        }

        $users = $this->persist->find('user');
        foreach ($users as $user) {
            if (!isset($orders[$user['login']])) continue;
            $orderSum = $this->calcUserOrder($orders[$user['login']]['order'], $dishes);
            $user['balance'] = $user['balance'] - $orderSum;
            $this->persist->save('user', $user);
        }

        $this->persist->lockDate($date);
    }

    public function calcUserOrder($order, $dishes)
    {
        $sum = 0;
        foreach ($order as $item) {
            $dishId = $item['id'];
            $price = $dishes[$dishId]['price'] * $item['count'];
            $sum += $price;
        }

        return $sum;
    }

    public function generateReport($date)
    {
        $result = array();
        $dishes = $this->getAllDishes($date);
        $orders = $this->getAllOrders($date);
        $sum = 0;
        foreach ($dishes as $dish) {
            $dish['count'] = 0;
            $dish['amount'] = 0;
            foreach ($orders as $userOrder) {
                foreach ($userOrder['order'] as $order) {
                    if ($order['id'] == $dish['id']) {
                        $dish['count'] += $order['count'];
                        $dish['amount'] += round($order['count'] * $dish['price']);
                    }
                }
            }
            $sum += $dish['amount'];
            $result[] = $dish;
        }

        return array('orders' => $result, 'sum' => $sum);
    }

    public function generateUserReport($date)
    {
        $result = $orders = $dishes = array();
        foreach ($this->getAllOrders($date) as $item) {
            $orders[$item['user']] = $item;
        }
        foreach ($this->getAllDishes($date) as $item) {
            $dishes[$item['id']] = $item;
        }
        $users = $this->persist->find('user');
        $sum = 0;
        $sumForUsers = 0;

        foreach ($users as $user) {
            if (! isset($orders[$user['login']])) continue;
            $record = array(
                'login' => $user['login'],
                'user' => $user['name'],
                'orders' => array(),
                'total' => 0
            );

            foreach ($orders[$user['login']]['order'] as $order) {
                $record['orders'][] = array(
                    'dish' => $dishes[$order['id']],
                    'count' => $order['count']
                );
                $record['total'] += ($dishes[$order['id']]['price'] * $order['count']);
            }
            $sum += $record['total'];
            $sumForUsers += $record['total'];

            $result[] = $record;
        }

        return array('orders' => $result, 'sum' => $sum, 'sumForUsers' => $sumForUsers);
    }

    public function checkDate($date)
    {
        if ($this->persist->dateIsLocked($date)) {
            throw new \Exception('Замовити на ' .  $date->format(DATE_FORMAT) . " неможливо!\nЧас замовлення вичерпано.");
        }
    }

    public function getOrderDate()
    {
        $date = new \DateTime();
        if ($this->persist->dateIsLocked($date)) {
            $date = new \DateTime('+ 1 day');
        }

        return $date;
    }
}