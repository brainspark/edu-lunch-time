<?php
namespace LunchTime\Helper;

use LunchTime\Model\User as UserModel;

class User
{
    protected $persist;

    public function __construct($persist = null)
    {
        $this->persist = $persist;
    }

    function isValid($data, $fields)
    {
        foreach ($fields as $field) {
            if (! (isset($data[$field]) && '' != $data[$field])) {
                return false;
            }
        }

        return true;
    }

    public function getAllUsers($showPass = false)
    {
        $users = $this->persist->find('user');
        if (! $showPass) {
            foreach ($users as $k => $user) {
                unset($users[$k]['password']);
            }
        }

        return $users;
    }

    public function createUser($data)
    {
        if (! $this->isValid($data, array('login', 'name', 'password', 'balance'))) {
            throw new \Exception('Ти чо! Не правельно щось написав!');
        }
        $user = new UserModel($data['login'], $data['name'], $data['password'], $data['balance']);
        $this->persist->save('user', $user->toArray(true));

        return $user;
    }

    public function findUser($login)
    {
        $data = $this->persist->findOne('user', $login);
        if(false == $data)
            return false;

        return UserModel::createByArray($data);
    }

    public function updateUser($data)
    {
        if (! $this->isValid($data, array('login', 'name', 'password', 'balance'))) {
            throw new \Exception('Ти чо! Не правельно щось написав!');
        }
        $user = new UserModel($data['login'], $data['name'], $data['password'], $data['balance']);
        $this->persist->save('user', $user->toArray(true));

        return $user;
    }

    public function deleteUser($data)
    {
        $this->persist->delete('user', array('login' => $data));
    }

    public function correctPassword($login, $password)
    {
        $user = $this->findUser($login);

        if (! $user) {
            return false;
        }
        if ($password == $user->password) {
            return true;
        } else {
            return false;
        }
    }
} 