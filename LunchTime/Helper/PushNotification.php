<?php
namespace LunchTime\Helper;

use LunchTime\PushNotifications\Service\Notifications;
use LunchTime\PushNotifications\Message\iOSMessage,
    LunchTime\PushNotifications\Message\AndroidMessage;


class PushNotification
{
    private $push;

    public function __construct(Notifications $push)
    {
        $this->push = $push;
    }

    public function send($os, $deviceId, $text, $params)
    {
        switch ($os) {
            case 'ios': {
                $message = new iOSMessage();
                foreach ($params as $key => $value) {
                    $message->addCustomData($key, $value);
                }
                $message->setAPSSound('Ding.aiff');
                $message->setAPSBadge(1);
            } break;
            case 'android': {
                $message = new AndroidMessage();
                $message->setGCM(true);
                $message->setGCMOptions($params);
            } break;
            default: {
            return false;
            }
        }
        $message->setMessage($text);
        $message->setDeviceIdentifier($deviceId);

        try {
            return $this->push->send($message);
        } catch(\Exception $e) {
            return false;
        }
    }

    public function sendMessage($to, $subject, $body)
    {
        $params = array('title' => $subject, 'type' => 'message');

        return $this->send($to['os'], $to['deviceId'], $body, $params);
    }

    public function sendMessages($devices, $subject, $body)
    {
        foreach ($devices as $device) {
            $this->sendMessage($device, $subject, $body);
        }
    }
} 