<?php
namespace LunchTime\Helper;

use Symfony\Component\HttpFoundation\JsonResponse;

class ApiResponse
{
    private static $messages = array(
        0 => 'Помилка',
        1 => 'OK',
        4 => 'Користувача не знайдено!',
        5 => 'Час замовлень вичерпано!',
        6 => 'Меню ще не готово',
        7 => 'Немає замовлень для замовлення',
        8 => 'Код верифікації невірний',
        9 => 'Файл повинен бути з роширенням: jpg, jpeg, png'
    );

    public function __construct()
    {

    }

    public function parse($data)
    {
        if (get_magic_quotes_gpc()) {
            $data = stripslashes($data);
        }
        var_dump(get_magic_quotes_gpc());

        return json_decode($data, true);
    }

    public function getResponse($obj, $code = 200)
    {
        return new JsonResponse($obj, $code);
    }

    public function response($code, $params = array(), $message = null)
    {
        if(empty($message)) {
            $message = self::$messages[$code];
        }
        $responseArray = array('code' => $code, "message" => $message);
        if(is_array($params)) {
            $responseArray = array_merge($responseArray, $params);
        }

        return $this->getResponse($responseArray);
    }
}