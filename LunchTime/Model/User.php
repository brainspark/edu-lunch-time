<?php
namespace LunchTime\Model;

class User
{
    public
        $login,
        $name,
        $password,
        $balance;

    public function __construct($login, $name, $password, $balance = 0)
    {
        $this->login    = $login;
        $this->name     = $name;
        $this->password = $password;
        $this->balance  = $balance;
    }

    public static function createByArray($data)
    {
        return new User($data['login'], $data['name'], $data['password'], $data['balance']);
    }

    public function debiting($sum)
    {
        $result = $this->balance - $sum;
        if($result < 0) {
            throw new \Exception('Вай! Недостатньо коштів!');
        } else {
            $this->balance = $result;

            return $this->balance;
        }
    }

    public function refill($sum)
    {
        $this->balance += $sum;
    }

    public function toArray($isShowPass = false)
    {
        $user =  array(
            'login'   => $this->login,
            'name'    => $this->name,
            'balance' => $this->balance
        );
        if ($isShowPass) {
            $user['password'] = $this->password;
        }

        return $user;
    }

    public function toResponse()
    {
        return array('user' => $this);
    }
} 