<?php

$app->mount('/', new \LunchTime\Controller\IndexController());
$app->mount('/admin', new \LunchTime\Controller\AdminController());

$app->mount('/api', new \LunchTime\Controller\ApiController());
$app->mount('/api/admin', new \LunchTime\Controller\ApiAdminController());