<?php
require 'environment.php';

# app's default date format
define('DATE_FORMAT', 'd-m-Y');

# session
$app->register(new Silex\Provider\SessionServiceProvider());

# Buzz
$app['autoloader']->add('Buzz', __DIR__.'/vendor/buzz/lib');
$app['params'] = $params;

# twig
$app->register(new Silex\Provider\TwigServiceProvider(), array(
        "twig.path" => APP_NAME . "/Resources/views",
        'twig.options' => array('cache' => APP_NAME . '/Resources/cache/twig', 'auto_reload' => true)
    )
);
$app["twig"] = $app->share($app->extend("twig", function (\Twig_Environment $twig, Silex\Application $app) {
//    $twig->addExtension(new \Exaloc\Helper\CorrectScoreExtension($app));
    return $twig;
}));

# url generator
$app->register(new Silex\Provider\UrlGeneratorServiceProvider());

# validation
$app->register(new Silex\Provider\ValidatorServiceProvider());

# security
$app->register(new Silex\Provider\SecurityServiceProvider(), array(
    'security.firewalls' => array(
        'admin' => array(
            'pattern' => '^/(admin|api/admin(?!/groupOrder))', // cron job - url groupOrder
            'http' => true,
            'users' => array(
                'admin' => array('ROLE_ADMIN', '74913f5cd5f61ec0bcfdb775414c2fb3d161b620'),
            )
        )
    ),
    'security.encoder.digest' => new \Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder('sha1', false, 1)
));

# security
$app->register(new Silex\Provider\SecurityServiceProvider(), array(
    'security.firewalls' => array(
        'admin' => array(
            'pattern' => '^/(admin|api/admin(?!/groupOrder))', // cron job - url groupOrder
            'http' => true,
            'users' => array(
                'admin' => array('ROLE_ADMIN', '74913f5cd5f61ec0bcfdb775414c2fb3d161b620'),
            )
        )
    ),
    'security.encoder.digest' => new \Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder('sha1', false, 1)
));

$app->boot();

// --------------------------------------------- Services ---------------------------------------------------
# Persister
$app['persister'] = $app->share(function() use ($app) {
    return new LunchTime\Helper\Persister();
});

# Mail
$app['mail'] = $app->share(function() use ($app) {
    return new LunchTime\Helper\Mail($app["twig"], $app['params']['emails']);
});

# Dish Helper
$app['dish'] = $app->share(function() use ($app) {
    return new LunchTime\Helper\Dish($app['persister']);
});

$app['user'] = $app->share(function() use ($app) {
    return new LunchTime\Helper\User($app['persister']);
});

$app['api_response'] = $app->share(function() use ($app) {
    return new LunchTime\Helper\ApiResponse();
});

$app['push_service'] = $app->share(function() use ($app) {
    return new \LunchTime\PushNotifications\Service\Notifications($app['params']['push']);
});
$app['push_notification'] = $app->share(function() use ($app) {
    return new \LunchTime\Helper\PushNotification($app['push_service']);
});

// ---------------------------------------------         ---------------------------------------------------

#translations
require APP_NAME . '/Resources/config/translations.php';

# routers
require APP_NAME . '/Resources/config/routers.php';