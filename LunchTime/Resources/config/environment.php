<?php
$env = false; // default in case
if(!empty($_SERVER['ENVIRONMENT'])){
    $env = $_SERVER['ENVIRONMENT'];
}

define('PROJECT_NAME', 'LunchTime');
$params = array(
	'emails' => array(
        'maker' => 'natalia.dostavka@gmail.com',
        'admins' => 'o.pozyvailo@webkate.com,r.khomenko@webkate.com'
    ),
    /*'emails' => array(
        'maker' => 'jura@webkate.com',
        'admins' => 'jura@webkate.com,jura@webkate.com'
    ),*/
    'push' => array(
        'ios' => array('sandbox' => false, 'pem' => __DIR__.'/cert/apns-prod.pem', 'passphrase' => '1111'),
        'android' => array('apiKey' => 'AIzaSyBtXSqVOj3Q7RtjBCWY-igbKXbliH_uXqo', 'useMultiCurl' => true)
    )
);

switch($env){
    case 'development':
        define('BASE_URL', 'http://lunch-time.local');

        function v($object = null){
            echo '<pre>';
            var_dump($object);
            echo '</pre>';
        }

        error_reporting(E_ALL);
        ini_set('display_errors', 1);
        break;

    case 'staging':

        break;

    default: // production
        define('BASE_URL', 'http://lunchtime.webkate.com');

        break;
}