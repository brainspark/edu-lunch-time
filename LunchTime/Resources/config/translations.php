<?php
# trans
$app->register(new Silex\Provider\TranslationServiceProvider(),
    array(
        "locale_fallback" => "en"
    )
);
$en = array(

);
$app['translator.domains'] = array(
    'messages' => array(
        'en' => $en,
    ),
);

if (!$lang = $app['session']->get('_locale')) {
    $lang = 'en';
}

$app['locale'] = $lang;
$app['translator']->setLocale($lang);