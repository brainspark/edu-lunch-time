var App = {
    userLogin: null,
    order: [],
    dishes: [],

    init: function (callback) {
        App.bindScrollTop();
        if (callback !== undefined) {
            callback();
        }
    },

    today: function () {
        var date = new Date();
        return date.getDay() + '-' + date.getMonth() + '-' + date.getYear();
    },

    loadMainDishes: function (date) {
        var date = (undefined == date)? '' : '?date='+date, contentElm = $('#dishes');
        contentElm.html('<h3>Завантаження...</h3>');
        $.get('/api/dishes'+date, function(data){
            if (data.code == 1) {
                var dishBlocks = '';
                jsonData = data.dishes;
                setNormDishes(jsonData);
                for (var i in jsonData) {
                    var info = jsonData[i].description;
                    var tooltip = ('' != info && null != info)? ' data-toggle="tooltip" data-placement="bottom" title="'+info+'"' : '';
                    dishBlocks += '<div class="col-sm-4 col-xs-6 block-inline"> \
                        <div class="panel panel-default">\
                            <div class="cat">'+jsonData[i].category+'</div>\
                            <div class="panel-thumbnail">\
                                <img src="'+jsonData[i].image+'" class="img-responsive">\
                                <button data-id="'+jsonData[i].id+'" class="btn btn-success eat-me">З\'їсти!\
                                <span class="count"></span>\
                                </button>\
                             </div>\
                        <div class="panel-body"'+tooltip+'>\
                         <span class="price">'+jsonData[i].price+'</span>\
                            <h2>'+jsonData[i].name+'</h2>\
                            <small>'+jsonData[i].tags.join(", ")+'</small>\
                         </div>\
                        </div>\
                     </div>'
                }
            } else {
                dishBlocks = '<h3>' + data.message + '</h3>';
            }
            contentElm.html(dishBlocks);
            $('[data-toggle="tooltip"]').tooltip({ delay: { show: 200, hide: 80 } });
        });
    },

    /**
     * Add Dishes To Order
     */
    bindAddDish2Order: function () {
        $('#dishes').on('click', function(event){
            if(event.target.classList.contains('eat-me')){
                addDishToOrder(event.target.getAttribute('data-id')*1);
                $('#plate .count').html(getCountDishesInOrder());

                event.target.classList.remove('btn-success');
                event.target.classList.add('btn-primary');

                showAddedDishes();
            }
        });
    },

    /**
     *  Add USer To Order
     */
    bindAddUser2Order: function () {
        $('#nav').on('click', function(event){
            if(event.target.classList.contains('text')){
                userName = $('#nav .filter-option').html();
                var regexp = /\((.*?)\)/gi;
                userName = userName.match(regexp);
                userName = userName[0].replace('(', '').replace(')', '');
                App.userLogin = userName;
            }
        });
    },

    bindPlate: function () {
        $('#plate').on('click', function(){
            $('#plate .pop-up').show();
        });
        $('#plate i').on('click', function(e){
            e.stopPropagation();
            $('#plate .pop-up').hide();
        });
    },

    bindScrollTop: function () {
        $('.scroll-top').click(function(){
            $('body,html').animate({scrollTop:0},300);
        });
    },

    makeOrder: function() {
        App.userLogin = getUserName();
        if(null == App.userLogin) {
            alert('Виберіть себе зі списку голодних!');
            return;
        }
        if(0 == App.order.length) {
            alert('Зробіть замовлення!');
            return;
        }
        var codeVerification, code;
        BootstrapDialog.show({
            title: 'Замовлення',
            message:
                '<label>Введіть Ваш код верифікації для здійсненя замовлення:</label><br>'+
                '<div class="col-sm-6"><input type="text" class="form-control" id="codeVerification" placeholder="код верифікації"></div>' +
                '<br><br><br><em class="text-info small noteDialog"">// Примітка: Для зміни замовлення, просто зробіть інше (нове) замовлення на ту ж дату, і старе перезатреться.</em>',
            buttons: [
                { label: 'Передумати', action: function(d) { d.close(); }},
                { label: 'Замовити', cssClass: 'btn-primary', action: function(d) {
                    var data = { data: { user: App.userLogin, code: code, order: App.order, date: $('#selectData').val() } };
                    $.post('/api/orders', data, function (data) {
                        if(data.code == 1) {
                            alert('Замовлено!');
                            window.location.reload();
                        } else {
                            ErrorApp.show(data);
                            if(data.code == 8) codeVerification.val('');
                        }
                    });
                }}
            ],
            onshow: function(dialogRef){
                setTimeout(function() {
                    codeVerification = $('#codeVerification');
                    codeVerification.change(function() { code = $(this).val(); });
                }, 300);
            }
        });
    },

    showAllOrdersByUser: function (date) {
        var urlReport = 'http://'+window.location.host+'/user-report/';
        BootstrapDialog.show({
            title: 'Замовлення співробітників на <input type="text" id="selectDataDialog" class="datepicker" value="'+date+'">',
            message: '<iframe id="reportUserIframe" src='+urlReport+date+'></iframe>',
            buttons: [
                { label: '<span class="glyphicon glyphicon-print"></span>', action: function(d) { printFrame('reportUserIframe'); }},
                { label: 'ОК', action: function(dialog) { dialog.close(); } }
            ],
            onshow: function(dialogRef){
                bodyBlocked(true);
                setTimeout(function() {
                    var selectDate = $('#selectDataDialog');
                    selectDate.datepicker(Params.datepicker);
                    selectDate.change(function() { $('#reportUserIframe').attr('src', urlReport+selectDate.val()); });
                }, 300);
            }
        });
    },

    dateGroupOrders: function () {
        $.get('/api/dateLocks', function(data){
            var content = '';
            for (var i = 0; i < data.locks.length; i++)
                content += '<option>'+data.locks[i]+'</option>';
            $('#dateLocks').html(content);
        });
    },

    pushNotification: function (obj, type) {
        preloader.on();
        $(obj).attr('disabled', 'disabled');
        $.post('/api/push/'+type, function(data){
            $(obj).removeAttr('disabled');
            preloader.off('disabled');
            alert(data.message);
        });
    }
}

var AppAdmin = {
    loadDishes: function (date) {
        var date = (undefined == date)? '' : '?date='+date, contentElm = $('#create-menu-content');
        contentElm.html('<div class="row">Завантаження...</div>');
        $.get('/api/admin/dishes'+date, function(data){
            var content = '';
            for (var i = 0; i < data.base.dishes.length; i++) {
                content += '<div class="row'+(data.current[data.base.dishes[i].id] !== undefined ? ' active"' : '')+'">\
							<div class="col-lg-1">\
								<input type="checkbox"'+(data.current[data.base.dishes[i].id] !== undefined ? ' checked="checked"' : '')+' id="d'+data.base.dishes[i].id+'" name="dishes['+i+'][id]" value="'+data.base.dishes[i].id+'"/>\
								<label for="d'+data.base.dishes[i].id+'"></label>\
							</div>\
							<div class="col-lg-9 name">'+data.base.dishes[i].name+'</div>\
							<div class="col-lg-2 price">\
							    <input type="number" class="form-control" name="dishes['+i+'][price]" value="'+(data.current[data.base.dishes[i].id] !== undefined ? data.current[data.base.dishes[i].id].price : data.base.dishes[i].price)+'">\
							</div>\
						</div>';
            };
            contentElm.html(content);

            $('label').on('click', function () {
                $(this).parent().parent().toggleClass('active');
            });
        });
    },

    loadBaseDishes: function () {
        $.get('/api/admin/dishes', function(data){
            var content = '';
            for (var i = 0; i < data.base.dishes.length; i++) {
                var dish = data.base.dishes[i];
                var imgTeg = (null != dish.image && undefined != dish.image && '' != dish.image)? '<img width="100" src="/'+dish.image+'"><br>' : '';
                content += '<tr data-id="'+dish.id+'">\
                    <td class="colName">\
                        <input type="hidden" name="dishes['+i+'][id]" id="dishes_id_'+dish.id+'" value="'+dish.id+'"/>\
                        <input type="text" name="dishes['+i+'][name]" id="dishes_name_'+dish.id+'" value="'+dish.name+'"/><br>\
                        <textarea name="dishes['+i+'][description]" id="dishes_description_'+dish.id+'">'+dish.description+'</textarea>\
                    </td>\
                    <td><input type="text" name="dishes['+i+'][category]" id="dishes_category_'+dish.id+'" value="'+dish.category+'"/></td>\
                    <td><input type="number" step="0.01" name="dishes['+i+'][price]" id="dishes_price_'+dish.id+'" value="'+dish.price+'"/></td>\
                    <td>'+imgTeg+'\
                        <input type="hidden" name="dishes['+i+'][image]" id="dishes_image_'+i+'" value="'+dish.image+'"/>\
                        <a href="#choosePhoto" id="choosePhotoBtn">Обрати фото</a>\
                    </td>\
                    <td><textarea name="dishes['+i+'][tags]" id="dishes_tags_'+dish.id+'">'+dish.tags.join(",")+'</textarea></td>\
                    <td><a class="glyphicon glyphicon-trash" href="#delete" id="deleteDishBtn"></a></td>';
            };
            $('#dishes-content').html(content);

            $('label').on('click', function () {
                $(this).parent().parent().toggleClass('active');
            });
        });
    },

    createDish: function(){
        var form = $('#menu_form').serialize();
        $.post('/api/admin/dishes', form, function (content) {
            if (content.code == 1) {
                AppAdmin.loadDishes($('#menu_form #date').val());
            } else {
                ErrorApp.show(content);
            }
        })
    },

    deleteOrder: function(id, date){
        $.post('/api/admin/order/delete', {id: id, date: date }, function (content) {
            if (content.code == 1) {
                window.location.reload();
            } else {
                ErrorApp.show(content);
            }
        })
    },

    createGroupOrder: function(){
        var date = $('#dateGroupOrder').val(), data = (undefined == date)? {} : { date: date };
        $.post('/api/admin/groupOrder', data, function (content) {
            if (content.code == 1) {
                App.dateGroupOrders();
                alert('Замовлено!');
            } else {
                ErrorApp.show(content);
            }
        })
    },

    updateDishList:function(){
        var form = $('#dishes_form').serialize();
        $.post('/api/admin/dishes_list', form, function (content) {
            if (content.code == 1) {
                window.location.reload();
            } else {
                ErrorApp.show(content);
            }
        })
    },

    addBaseDishes: function () {
        var id = $($('#dishes_form table tbody tr').last()[0]).data('id') + 1;
        var content = '<tr data-id="'+id+'">\
            <td class="colName">\
                <input type="hidden" name="dishes['+id+'][id]" id="dishes_id_'+id+'" value="'+id+'"/>\
                <input type="text" name="dishes['+id+'][name]" id="dishes_name_'+id+'" value=""/><br>\
                <textarea name="dishes['+id+'][description]" id="dishes_description_'+id+'"></textarea>\
            </td>\
            <td><input type="text" name="dishes['+id+'][category]" id="dishes_category_'+id+'" value=""/></td>\
            <td><input type="number" step="0.01" name="dishes['+id+'][price]" id="dishes_price_'+id+'" value=""/></td>\
            <td>\
                <input type="hidden" name="dishes['+id+'][image]" id="dishes_image_'+id+'" value=""/>\
                <a id="choosePhotoBtn">Обрати фото</a>\
            </td>\
            <td><textarea name="dishes['+id+'][tags]" id="dishes_tags_'+id+'"></textarea></td>\
            <td><a class="glyphicon glyphicon-trash" href="#delete" id="deleteDishBtn"></a></td>';
        $('#dishes_form table tbody').append(content);
    },

    loadUsers: function () {
        $.get('/api/admin/users', function(data){
            var content = '';
            for (var i = 0; i < data.users.length; i++) {
                var user = data.users[i], color = '';
                if(user.balance < 0)
                    color = ' class="danger"';
                else if(user.balance < 10)
                    color = ' class="warning"';
                content += '<tr data-id="'+user.login+'"'+color+'>\
                    <td>'+user.login+'</td>\
                    <td>'+user.name+'</td>\
                    <td>'+user.password+'</td>\
                    <td>'+user.balance+'</td>\
                    <td>\
                        <a class="glyphicon glyphicon-pencil" href="#edit" id="editUserBtn"></a>\
                        <a class="glyphicon glyphicon-trash" href="#delete" id="deleteUserBtn"></a>\
                    </td>\
                ';
            };
            $('#users-content').html(content);

            $('label').on('click', function () {
                $(this).parent().parent().toggleClass('active');
            });
            //$('table').dataTable( { paging: false, ordering: false, info: false, bDestroy: true });
        });
    },

    addUser: function(dialog){
        var form = $('#formAddUser').serialize();
        $.post('/api/admin/users', form, function (content) {
            if (content.code == 1) {
                AppAdmin.loadUsers();
                dialog.close();
            } else {
                ErrorApp.show(content);
            }
        })
    },
    updateUser:function(dialog, login){
        var form = $('#formAddUser').serialize();
        $.post('/api/admin/users/' + login, form, function (content) {
            if (content.code == 1) {
                AppAdmin.loadUsers();
                dialog.close();
            } else {
                ErrorApp.show(content);
            }
        })
    },
    deleteUser:function(dialog, login){
        var form = $('#formAddUser').serialize();
        $.post('/api/admin/users/' + login + '/delete', form, function (content) {
            if (content.code == 1) {
                dialog.close();
            } else {
                ErrorApp.show(content);
            }
        })
    },

    showAllOrders: function (date) {
        var urlReport = 'http://'+window.location.host+'/report/';
        BootstrapDialog.show({
            title: 'Замовлення на <input type="text" id="selectDataDialog" class="datepicker" value="'+date+'">',
            message: '<iframe id="reportIframe" src='+urlReport+date+'></iframe>',
            buttons: [
                { label: '<span class="glyphicon glyphicon-print"></span>', action: function(d) { printFrame('reportIframe'); }},
                { label: 'ОК', action: function(d) { d.close(); } },
            ],
            onshow: function(dialogRef){
                bodyBlocked(true);
                setTimeout(function() {
                    var selectDate = $('#selectDataDialog');
                    selectDate.datepicker(Params.datepicker);
                    selectDate.change(function() { $('#reportIframe').attr('src', urlReport+selectDate.val()); });
                }, 300);
            },
            onhidden: function() {

            }
        });
    },

    showAllImages:function(){
        var bl = $('.choosePhoto'), content = '';
        bl.html('<h3>Завантаження...</h3>');
        $.get('/api/admin/images', function (data) {
            $.each(data.images, function(i, v) {
                content += '<div><img src="/'+v+'" data-src="'+v+'"><span>X</span></div>';
            });
            bl.html(content);
        })
    },

    choosePhoto: function (td) {
        var photoSrc = '';
        BootstrapDialog.show({
            title: 'Виберіть фото або <button id="uploadNewPhoto" class="btn btn-default">Завантажити нове</button>',
            message: '<div class="choosePhoto"></div><form id="formUploadPhoto" method="POST" action="/api/admin/image/upload" style="display: none;"><input type="file" name="photo"/></form>',
            buttons: [ { label: 'Передумати', action: function(d) { d.close(); }} ],
            onshow: function(d){
                bodyBlocked(true);
                setTimeout(function() {
                    AppAdmin.showAllImages();
                    $(".choosePhoto").on( "click", "div", function() {
                        $('.choosePhoto div').removeClass('selected');
                        $(this).addClass('selected');
                        photoSrc = $(this).find('img').data('src');
                        if ('' != photoSrc) {
                            var imgEl = td.find('img'); console.log(imgEl);
                            if (imgEl.length == 0)
                                td.prepend('<img width="100" src="/'+photoSrc+'"><br>');
                            else
                                imgEl.attr('src', '/'+photoSrc);
                            td.find('input').val(photoSrc);
                        }
                        d.close();
                    });
                    $(".choosePhoto").on( "click", "div span", function(e) {
                        var div = $(this).parent();
                        $.post('/api/admin/image/delete', { file: div.find('img').data('src') }, function (data) { })
                        div.remove();
                        e.stoppropagation();
                    });

                    $('#formUploadPhoto').ajaxForm(function(data) {
                        $('#formUploadPhoto input').val('');
                        if (1 == data.code)
                            AppAdmin.showAllImages();
                        else
                            alert(data.message);
                        preloader.off('disabled');
                    });
                    $('#formUploadPhoto input').change(function() {
                        if('' != $(this).val()) {
                            $('#formUploadPhoto').submit();
                            preloader.on();
                        }
                    });
                    $('#uploadNewPhoto').click(function() {
                        $('#formUploadPhoto input').click();
                    });
                }, 300);
            }
        });
    }
}

var ErrorApp = {
    show:function(msg){
        alert(msg.message);
    }
}

var Params = {
    datepicker: { format: 'dd-mm-yyyy', language: 'uk', todayBtn: 'linked', autoclose: true }
}
var preloader = new $.materialPreloader({
    position: 'top',
    height: '5px',
    col_1: '#159756',
    col_2: '#da4733',
    col_3: '#3b78e7',
    col_4: '#fdba2c',
    fadeIn: 200,
    fadeOut: 200,
});

$(document).ready(function () {
    $('.datepicker').datepicker(Params.datepicker);
});