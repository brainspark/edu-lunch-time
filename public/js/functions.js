function addDishToOrder(id) {
    for (var i = 0; i < App.order.length; i++) {
        if (App.order[i].id == id) {
            App.order[i].count++;
            App.order[i].price += App.dishes[id].price *1;
            showCountIcon(id, i);
            return;
        }
    }
    App.order.push({id: id, count: 1, price: App.dishes[id].price *1});
}

function setNormDishes(jsonData) {
    $.each(jsonData, function(i, v) { App.dishes[v.id] = v; } );
}

function showCountIcon(id, index) {
    $('#dishes [data-id="' + id + '"] .count')
        .show()
        .html(App.order[index].count);
}

function getCountDishesInOrder() {
    var summ = 0;
    for (var j = 0; j < App.order.length; j++) {
        if (App.order[j]) summ += App.order[j].count;
    }
    return summ;
}

function showAddedDishes() {
    if (App.order.length) {
        var tBody = '';
        var totalPrice = 0;
        for (var i = 0; i < App.order.length; i++) {
            totalPrice += App.order[i].price;
            tBody += '<tr>\
                            <td>' + App.dishes[App.order[i].id].name + '</td>\
                            <td>' + App.order[i].count + '</td>\
                            <td>' + App.order[i].price + '</td>\
                            <td><i title="Видалити" class="glyphicon glyphicon-remove" data-index="' + i + '"></i></td>\
                          </tr>';
            $('#plate .pop-up span').html('<table class="table table-striped">\
                                                    <thead>\
                                                        <tr>\
                                                            <th>Блюдо</th>\
                                                            <th>Кіл.</th>\
                                                            <th>Ціна</th>\
                                                            <th></th>\
                                                        </tr>\
                                                     </thead>\
                                                    <tbody>\
                                                    ' + tBody + '\
                                                        <tr class="total">\
                                                            <td>Ви платите</td>\
                                                            <td></td>\
                                                            <td>' + (totalPrice).toFixed(2) + '</td>\
                                                            <td></td>\
                                                        </tr>\
                                                     </tbody>\
                                               </table>'
            );
        }
        //Set OnClick on i elements
        $('#plate table i').on('click', function () {
            deleteDish(this.getAttribute('data-index') * 1);
        });
    } else {
        $('#plate .pop-up span').html('зараз пусто <b>:(</b>');
        $('#plate .count').html(0);
    }
}

function deleteDish(index) {
    var id = App.order[index].id;
    App.order.splice(index, 1);
    showAddedDishes();
    //Change Buttons
    $('.panel [data-id="' + id + '"]')
        .removeClass('btn-primary')
        .addClass('btn-success');
    $('#dishes [data-id="' + id + '"] .count').hide();
    //Refresh Count Dishes in order
    $('#plate .count').html(getCountDishesInOrder());
}
function deleteAllDishes() {
    App.order = [];
    showAddedDishes();
}

function getFormAddUser()
{
    var form = '<form class="form-horizontal" id="formAddUser">\
        <div class="control-group">\
            <label class="control-label" for="login">Логін</label>\
            <div class="controls"><input type="text" id="user_login" name="user[login]" value=""></div>\
        </div>\
        <div class="control-group">\
            <label class="control-label" for="name">Пароль</label>\
            <div class="controls"><input type="text" id="user_name" name="user[password]" value=""></div>\
        </div>\
        <div class="control-group">\
            <label class="control-label" for="name">Ім\'я</label>\
            <div class="controls"><input type="text" id="user_name" name="user[name]" value=""></div>\
        </div>\
        <div class="control-group">\
            <label class="control-label" for="balance">Баланс</label>\
            <div class="controls"><input type="number" step="0.01" id="user_balance" name="user[balance]" value="0"></div>\
        </div>\
    </form>';

    return form;
}
function getFormUpdateUser(login, password, name, balance)
{
    var form = '<form class="form-horizontal" id="formAddUser">\
        <input type="hidden" id="user_login" name="user[login]" value="'+login+'"></div>\
        <div class="control-group">\
            <label class="control-label" for="name">Ім\'я</label>\
            <div class="controls"><input type="text" id="user_name" name="user[name]" value="'+name+'"></div>\
        </div>\
        <div class="control-group">\
            <label class="control-label" for="password">Пароль</label>\
            <div class="controls"><input type="text" id="user_password" name="user[password]" value="'+password+'"></div>\
        </div>\
        <div class="control-group">\
            <label class="control-label" for="balance">Баланс</label>\
            <div class="controls"><input type="number" step="0.01" id="user_balance" name="user[balance]" value="'+balance+'"></div>\
        </div>\
    </form>';

    return form;
}

function getUserName()
{
    userName = $('#nav .filter-option').html();
    var regexp = /\((.*?)\)/gi;
    userName = userName.match(regexp);
    if (null == userName) return null;
    userName = userName[0].replace('(', '').replace(')', '');

    return userName;
}

function printFrame(id) {
    var frm = document.getElementById(id).contentWindow;
    frm.focus();
    frm.print();

    return false;
}

function downloadIOSApp() {
    alert("Пароль для завантаження: hungry \n\r// Якщо застосунок не встановився, значить не доданий ваш девайс. Зверниться кудись..");
    window.location = "http://install.diawi.com/bGei2j"

    return false;
}

function bodyBlocked(isBlocked) {
    if(isBlocked)
        $('body').addClass('modal-open');
    else
        $('body').removeClass('modal-open');
}