/******************* ApplicationFunctional *********************/
var config, App, order = [], jsonData, userName;

config = {
    host:'http://lunch.local'
};
App = {
    createDish: function(){}
};


$(document).ready(function(){


    /******************** SentOrder *********************/
        $('#nav button').on('click', function(){
            if(order[0]){
                $('#mask').show();
            }
        });
    /******************* Activate spin.js ********************/
    var opts = {
        lines: 11, // The number of lines to draw
        length: 20, // The length of each line
        width: 15, // The line thickness
        radius: 30, // The radius of the inner circle
        corners: 1, // Corner roundness (0..1)
        rotate: 0, // The rotation offset
        direction: 1, // 1: clockwise, -1: counterclockwise
        color: '#000', // #rgb or #rrggbb or array of colors
        speed: 1, // Rounds per second
        trail: 60, // Afterglow percentage
        shadow: false, // Whether to render a shadow
        hwaccel: false, // Whether to use hardware acceleration
        className: 'spinner', // The CSS class to assign to the spinner
        zIndex: 2e9, // The z-index (defaults to 2000000000)
        top: '50%', // Top position relative to parent
        left: '50%' // Left position relative to parent
    };
    var target = document.getElementById('mask');
    var spinner = new Spinner(opts).spin(target);

    /******************* All VisualFunctional ********************/

    /* affix the navbar after scroll below header */
    //$('#nav').affix({
    //      offset: {
    //        top: $('header').height()-$('#nav').height()
    //      }
    //});

    /* highlight the top nav as scrolling occurs */
    //$('body').scrollspy({ target: '#nav' })

    /* smooth scrolling for scroll to top */


    /* smooth scrolling for nav sections */
    //$('#nav .navbar-nav li>a').click(function(){
    //  var link = $(this).attr('href');
    //  var posi = $(link).offset().top;
    //  $('body,html').animate({scrollTop:posi},700);
    //});


    $('#make-order').on('click', function () {
        var data = {
            data: {
                user: userName,
                order: order
            }
        };
        console.log(data);
        $.post(config.host + '/api/orders', data, function (content) {
            console.log(content);
        });
    });

});
