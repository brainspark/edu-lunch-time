<?php

// Param
$params = array('groupOrder', 'push_signUpForLunch');
if (!isset($argv[1]) || !in_array($argv[1], $params)) {
    echo "Error: incorrect params\n";
    echo "Usage: php cron.php [" . implode('|', $params) . "]\n";
    exit;
}

$url = 'http://lunchtime.webkate.com/api/';

switch ($argv[1]) {
    case 'groupOrder': {
        $url .= 'admin/groupOrder';
    } break;
    case 'push_signUpForLunch': {
        $url .= 'push/signUpForLunch';
    } break;
}

$body = '';
$c = curl_init ($url);
curl_setopt ($c, CURLOPT_POST, true);
curl_setopt ($c, CURLOPT_POSTFIELDS, $body);
curl_setopt ($c, CURLOPT_RETURNTRANSFER, true);
$page = curl_exec ($c);
curl_close ($c);

$text = date("Y-m-d H:i:s") . ' - ' . $argv[1] . ' - ' . $page . "\r\n";
file_put_contents(__DIR__.'/cron.log', $text, FILE_APPEND);

echo 1;