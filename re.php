<?php

function show_run($text, $command, $canFail = false)
{
    echo "\n* $text\n$command\n";
    passthru($command, $return);
    if (0 !== $return && !$canFail) {
        echo "\n/!\\ The command returned $return\n";
        exit(1);
    }
}


show_run("Destroying cache dir manually", "rm -rf public/temp/*");
show_run("Destroying cache dir manually", "rm -rf LunchTime/Resources/cache/*");
show_run("Changing permissions", "chmod -R 777 LunchTime/Resources/data");
show_run("Changing permissions", "chmod -R 777 img/*");


exit(0);
